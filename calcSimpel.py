import random
import sys
import sqlite3
from sqlite3 import Error
#from time \
import time

### TODO:
# Save Players to a sqlite db
# keep track of achievements
# e.g. longest series of correct calculations
# time calculations
# Make the operators a cli parameter
# Or make a config cli ui

result = 0  # Hält das Ergebnis der Rechnung
greeting = "Hallo {}. Hier sollst du rechnen üben, viel Spaß dabei!"
programStatus = "active"
okayCounter = 1  # Zählt wie viele Rechnungen in Folge Rechtig waren
globalCounter = 1  # Zählt wie viele Rechnungen schon gerechnet wurden
operatoren = ["+", "-", "*", ]
dbFile = 'game.db'
record = 0


def gen_calc(operator):
    global result
    if operator == "+":
        faktor1 = random.randint(0, 99)
        faktor2 = random.randint(1, 100 - faktor1)
        result = faktor1 + faktor2
    elif operator == "-":
        faktor1 = random.randint(1, 100)
        faktor2 = random.randint(1, 100)
        if (faktor2 > faktor1):
            tmp = faktor1
            faktor1 = faktor2
            faktor2 = tmp
        result = faktor1 - faktor2
    elif operator == "*":
        faktor1 = random.randint(1, 10)
        faktor2 = random.randint(1, 10)
        result = faktor1 * faktor2

    return "{}:  {} {} {} = ".format(globalCounter, faktor1, operator, faktor2)


def error_loop():
    global ergebnis, okayCounter
    print("Das war leider nicht richtig. Versuch es nochmal.")
    try:
        ergebnis = int(input(calculation))
    except ValueError:
        error_loop()
    okayCounter = 1


def create_connection(db_file):
    """ creates a database connection to a SQLite DB specified by
    the database file "
    :param db_file: databse file
    :return: connection object or None """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def load_gamer(gamerName):
    conn = create_connection('game.db')
    c = conn.cursor()
    c.execute('SELECT * FROM spieler WHERE name = ?', (gamerName,))
    row = c.fetchall()
    if len(row) == 1:
        return row
    else:
        c.execute('SELECT * FROM spieler')
        rows = c.fetchall()
        rows = len(rows) + 1
        c.execute('INSERT INTO spieler VALUES (?, ?, ?, ?)', (rows, gamerName, 0, 0,))
        conn.commit()
        return load_gamer(gamerName)
    #c.close()


def update_calc_counter(globalCounter, gamerId, okayCounter):
    conn = create_connection(dbFile)
    c = conn.cursor()
    c.execute('UPDATE spieler SET totalCalculations = ?, streak = ? WHERE id = ?', (globalCounter, okayCounter, gamerId, ))
    conn.commit()

def get_calculation_time(time):
    """"
        returns a string with the time needed to do the calculation
        :param: time: time in seconds
        :return: timestring the time needed to do the calculation
    """
    if time/60 < 1 or time == 1:
        return "{} Sekunden".format(time)
    elif time/3600 > 1:
        return "mehr als eine Stunde"
    else:
        return "{} Minuten und {} Sekunden".format(int(time/60), time % 60)


if len(sys.argv) > 2:
    print("Too many arguments")
    print(*sys.argv[1:], sep="; ")
    print("Übergeben Sie nur einen Spielername")
    exit(0)
elif len(sys.argv) == 1:
    print("Es wurde kein Spielername übergeben")
    exit(0)
else:
    gamer = load_gamer(sys.argv[1])#[0]
    gamerId = int(gamer[0][0])
    globalCounter = int(gamer[0][2])
    okayCounter = int(gamer[0][3])
    print(greeting.format(sys.argv[1]))

while programStatus == "active":
    for i in range(0, 10):
        #op =
        if result == 0:
            calculation = gen_calc(operatoren[random.randint(0, len(operatoren) - 1)] ) # Generates the calculation
            try:
                start_time = time.time()
                ergebnis = int(input(calculation))
            except ValueError:
                ergebnis = ""
            while result != ergebnis or ergebnis == "":
                error_loop()
            if okayCounter % 50 == 0 and okayCounter > 0:
                print(
                    "Du hast {} Rechnungen in Folge richtig gerechnet, SPITZE! Das kannst du dem Vati erzählen!".format(
                        okayCounter))
            elif okayCounter % 10 == 0 and okayCounter > 0:
                print("Du hast {} Rechnungen in Folge richtig gerechet, weiter so!".format(okayCounter))
            end_time = int(time.time()-start_time)

            print("Du hast {} für die Berechnung gebraucht.".format(get_calculation_time(end_time)))
            if record == 0:
                record = end_time
            if record != 0 and end_time < record:
                print("Das ist ein neuer Rerkord, bravo!")

            globalCounter += 1
            okayCounter += 1
            update_calc_counter(globalCounter, gamerId, okayCounter)
            result = 0

    weiter = input("Willst  du weiter machen? ")
    if (weiter.lower() != "ja" and weiter != "" and weiter.lower() != "j"):
        programStatus = "inactive"
