# cli.py
import click
import random

greeting = "Hallo {}. Hier sollst du rechnen üben, viel Spaß dabei!"
programStatus = "active"
result = 0

@click.command()
@click.argument('name')
def main(name):
    click.echo(greeting.format(name))
    global programStatus
    global result
    while programStatus == "active":
        while input(genCalc()) != result:
            click.echo("Das Ergebnis ist leider nicht richtig {}")#.format(name)

        if input("Willst du weiter machen: ") != "ja":
            programStatus = "stop"


def genCalc():
    faktor1 = random.randint(1,10)
    faktor2 = random.randint(1,10)
    global result
    result = faktor1 * faktor2
    return "Was ist das Ergebnis von \n {} * {} = ".format(faktor1, faktor2)


if __name__ == "__main__":
    main()

